
========
 DjRest
========


Initial setup and other information worth knowing.

.. contents::

Installation
============

Creation of the Python virtual environment
------------------------------------------

The next thing to do is the creation of the Python virtual environment with some required
packages. Simply execute::

 $ make

that will

a) initialize the environment with a set of required packages
b) initialize the site's database

You **must** remember to activate the virtual environment **before** doing anything::

 $ . env/bin/activate


Dependencies
------------

This project requires to work with celery, so we need Redis as message broker for celery. And we can start it with::

  $ make redis

This will download a docker for redis ready to use for us.


Celery
------

For running celery, we need to start the worker::

  $ make worker


Flower
------

For now is not installed but very useful.


Running
-------

After the initial setup is complete, you should have a runnable system that you can start
with::

  $ make runserver

or::

  $ make run

that should launch a web server accessible at the URL ``http://localhost:8000/``.


TESTS
=====

After the initial setup is complete, you can launch tests with::

  $ make test
