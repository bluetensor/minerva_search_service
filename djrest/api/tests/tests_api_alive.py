 from django.utils.module_loading import import_string
 
 from .base import BaseTestCase
 from ..views import BaseView, ApiKeyView, TrainView, StatusTaskView
 
 
 class ApiTestCase(BaseTestCase):
 
     def setUp(self):
         super(ApiTestCase, self).setUp()
         self.factory = APIRequestFactory()
 
     def test_alive(self):
         """Test basic alive api"""
 
         view = BaseView.as_view()
         request = self.factory.get('/isAlive')
         response = view(request)
         result = json.loads(response.content)
 
         self.assertTrue(response.status_code==200)
         self.assertTrue(result['status']=='OK')
 
    def test_create_api_key(self):
        """Test create api key"""

        view = ApiKeyView.as_view()
        import pdb; pdb.set_trace()
        request = self.factory.get('/create-apikey')
        response = view(request)
        result = json.loads(response.content)

        self.assertTrue(response.status_code==201)
        self.assertTrue(result['status']=='OK')

     def test_status_task(self):
         """Test status view"""
 
         view = StatusTaskView.as_view()
         request = self.factory.get('/status?task_id=123')
         response = view(request)
         result = json.loads(response.content)
 
         self.assertTrue(response.status_code==200)
         self.assertTrue(result['task_status']=='PENDING')
 
     def test_train_webhook(self):
         """Test train view and webhook api"""
 
         view = TrainView.as_view()
         url = 'http://localhost:8000/api/v1/isAlive'
         request = self.factory.post('/train',
                                     {'data': 'new data', 'callback': url},
                                     format='json')
         response = view(request)
         result = json.loads(response.content)
 
         self.assertTrue(response.status_code==200)
         self.assertTrue(result['status']=='OK')
         self.assertTrue(result['task_status']=='PENDING')
         self.assertFalse(result['task_id']==None)
 
         task_id = result['task_id']
 
         # wait a little bit for finishing task
         sleep(1)
 
         request = self.factory.get('/status?task_id=%s' % task_id)
         view = StatusTaskView.as_view()
         response = view(request)
         result = json.loads(response.content)
 
         self.assertTrue(response.status_code==200)
         self.assertTrue(result['task_status']=='SUCCESS')
