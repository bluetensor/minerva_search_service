# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template —
# :Created:   mar 1 giu 2021, 18:18:37
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

from django.test import TestCase
from rest_framework.test import APITestCase


class BaseTestCase(APITestCase):
    """
    All the project test mixin on top of Django test case class.
    """
