"""
Create permission groups
Create permissions (read only) to models for a set of groups
"""
import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from rest_framework_api_key.models import APIKey
from django.conf import settings

debug_logger = logging.getLogger('debug_logger')

class Command(BaseCommand):
    help = 'Creates apikeys'

    def handle(self, *args, **options):
        api_key, key = APIKey.objects.create_key()
        debug_logger.debug("API_KEY: "+key)

        print("Created default apikeys")
