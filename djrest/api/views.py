# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — Basic views/endpoints
# :Created:   mar 1 giu 2021, 18:42:19
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

import os
# import pandas as pd
import requests
import json
import logging

from django.http import HttpResponse, JsonResponse
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework_api_key.models import APIKey
from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.response import Response
from rest_framework.views import APIView
from djrest.api.gpt2.interactive_conditional_samples import interact_model
from django.conf import settings
from djrest.worker.utils import get_task_status, create_task
from djrest.worker.tasks import train_task
from gingerit.gingerit import GingerIt as parser


debug_logger = logging.getLogger('debug_logger')
error_logger = logging.getLogger('error_logger')


class BaseView(APIView):
    """Test alive API """

    def get(self, request):
        content = dict(status='OK')
        return JsonResponse(content, status=200)


class ApiKeyView(APIView):
    permission_classes = [AllowAny] # for testing purposes

    def get(self, request):
        try:
            api_key, key = APIKey.objects.create_key(name=settings.WEBSERVICE_NAME)

            content = {"api-key": key, "status":"OK"}

            resp = JsonResponse(content)
            resp.status_code = status.HTTP_201_CREATED
            return resp
        except Exception as e:
            print("This exception occurred: ", e)
            resp = JsonResponse({"Message": "An Internal Server Error occurred. Please contact the server admin."})
            resp.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return resp


class TrainView(APIView):
    """
    Train API
    """

    def post(self, request):
        """
        Start task for training data
        """
        data = request.data.get('data', None)
        # callback = request.data.get('callback', None)

        if data is None:# or callback is None:
            content = dict(status='Failed', message='Missing data or callback')
            return JsonResponse(content, status=200)

        # task = create_task(train_task, data, callback)
        risposta, intermediaHW, biblio = train_task(data)

        # answer=interact_model(raw_text=data)
        # answer=""
        # for indice in content[0]:
        #     answer=answer+(content[0][indice]['text']+", ")
        # answer=answer[0:len(answer)-2]

        print("RISPOSTA",risposta)
        print("INTERMEDIA",intermediaHW)
       
        # corrected_text = parser().parse(risposta)
        # answer=corrected_text['result']
        # print("DOPO",answer)
        # answer=interact_model(raw_text=answer)

        # print("GPT2",answer)
        return_value = {
            "doc_list": biblio,
            "search_string": data,
            "answer": risposta
        }

        # content = dict(status='OK',
        #                task_id=task.id,
        #                task_status=task.status)
        
        # from celery.contrib import rdb
        # rdb.set_trace()
        # print(return_value)
    
        return JsonResponse(return_value, status=200)


class StatusTaskView(APIView):
    """
    Status API
    """

    def get(self, request):
        """
        Get status of a celery task
        """
        task_id = request.query_params.get('task_id', None)
        if task_id is None:
            content = dict(status='Failed', message='Missing task id')
            return JsonResponse(content, status=200)

        status = get_task_status(task_id)

        content = dict(task_status=status)
        return JsonResponse(content, status=200)
