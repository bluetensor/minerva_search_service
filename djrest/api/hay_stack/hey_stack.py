from djrest.api.gpt2.interactive_conditional_samples import interact_model
from djrest.api.hay_stack.haystack.document_stores import ElasticsearchDocumentStore
from djrest.api.hay_stack.haystack.nodes import ElasticsearchRetriever
from djrest.api.hay_stack.haystack.nodes import FARMReader
from djrest.api.hay_stack.haystack.pipelines import ExtractiveQAPipeline
from djrest.api.hay_stack.haystack.utils import clean_wiki_text, convert_files_to_dicts, fetch_archive_from_http, print_answers
from djrest.api.hay_stack.haystack.utils import launch_es


def getAnswer(question, doc_dir):
    document_store = ElasticsearchDocumentStore(host="localhost", username="", password="", index="document")

    # Convert files to dicts
    # You can optionally supply a cleaning function that is applied to each doc (e.g. to remove footers)
    # It must take a str as input, and return a str.
    dicts = convert_files_to_dicts(dir_path=doc_dir, clean_func=clean_wiki_text, split_paragraphs=True)

    # We now have a list of dictionaries that we can write to our document store.
    # If your texts come from a different source (e.g. a DB), you can of course skip convert_files_to_dicts() and create the dictionaries yourself.
    # The default format here is:
    # {
    #    'text': "<DOCUMENT_TEXT_HERE>",
    #    'meta': {'name': "<DOCUMENT_NAME_HERE>", ...}
    #}
    # (Optionally: you can also add more key-value-pairs here, that will be indexed as fields in Elasticsearch and
    # can be accessed later for filtering or shown in the responses of the Pipeline)

    # Now, let's write the dicts containing documents to our DB.

    document_store.write_documents(dicts)

    retriever = ElasticsearchRetriever(document_store=document_store)

    reader = FARMReader(model_name_or_path="deepset/roberta-base-squad2", use_gpu=True)
    pipe = ExtractiveQAPipeline(reader, retriever)

    params = {"Retriever": {"top_k": 5}, "Reader": {"top_k": 10}}
    prediction = pipe.run(query=question, params=params)

    answers = prediction['answers']

    if len(answers) > 0:
        raw_text = answers[0].answer
        result_answer = interact_model(raw_text = raw_text)

        print("answer HAYSTACK : %s" % raw_text)
        print("answer GPT2: %s " % result_answer)
    else:
        result_answer = 'ERROR'

    used = []
    result = dict()
    sum=0.0
    counter=1
    for answer in answers:
        sum=sum+answer.score
        print("intermediate sum:",sum)
        if(counter>=5):
            break
        else:
            counter=counter+1
    for answer in answers:
        file_name = answer.meta['name']
        if file_name not in used:
            used.append(file_name)
            # key = file_name.replace(".txt", "")
            result[file_name] = dict(tf_idf_score=answer.score,weight=round(answer.score/sum,3), file_name=file_name)

    prediction_answer = answers[0].answer

    return result_answer, prediction_answer, result
