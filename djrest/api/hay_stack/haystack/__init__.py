import logging

# This configuration must be done before any import to apply to all submodules
logging.basicConfig(format="%(levelname)s - %(name)s -  %(message)s", datefmt="%m/%d/%Y %H:%M:%S", level=logging.WARNING)
logging.getLogger("haystack").setLevel(logging.INFO)

from djrest.api.hay_stack.haystack import pipelines
# from djrest.api.hay_stack.haystack.schema import Document, Answer, Label, MultiLabel, Span
# from djrest.api.hay_stack.haystack.nodes import BaseComponent
# from djrest.api.hay_stack.haystack.pipelines import Pipeline
from djrest.api.hay_stack.haystack._version import __version__

import pandas as pd
pd.options.display.max_colwidth = 80


# ###########################################
# Enable old style imports (temporary)
import sys

logger = logging.getLogger(__name__)

# Wrapper emitting a warning on import
def DeprecatedModule(mod, deprecated_attributes=None, is_module_deprecated=True):
    """ 
    Return a wrapped object that warns about deprecated accesses at import 
    """
    class DeprecationWrapper(object):
        warned = []
        
        def __getattr__(self, attr):
            is_a_deprecated_attr = deprecated_attributes and attr in deprecated_attributes
            is_a_deprecated_module = is_module_deprecated and attr not in ["__path__", "__spec__", "__name__"]
            warning_already_emitted = attr in self.warned
            attribute_exists = getattr(mod, attr) is not None

            if (is_a_deprecated_attr or is_a_deprecated_module) and not warning_already_emitted and attribute_exists:
                logger.warn(f"Object '{attr}' is imported through a deprecated path. Please check out the docs for the new import path.")
                self.warned.append(attr)
            return getattr(mod, attr)

    return DeprecationWrapper()

# All modules to be aliased need to be imported here
import djrest.api.hay_stack.haystack
from djrest.api.hay_stack.haystack.nodes import (
    connector, 
    document_classifier, 
    extractor, 
    file_converter, 
    answer_generator as generator,
    preprocessor,
    question_generator,
    ranker,
    reader,
    retriever,
    summarizer,
    translator
)
from djrest.api.hay_stack.haystack import document_stores
from djrest.api.hay_stack.haystack.nodes.retriever import text2sparql as graph_retriever
from djrest.api.hay_stack.haystack.document_stores import graphdb as knowledge_graph
from djrest.api.hay_stack.haystack.modeling.evaluation import eval
from djrest.api.hay_stack.haystack.modeling.logger import MLFlowLogger, StdoutLogger, TensorBoardLogger
from djrest.api.hay_stack.haystack.nodes.other import JoinDocuments, Docs2Answers
from djrest.api.hay_stack.haystack.nodes.query_classifier import SklearnQueryClassifier, TransformersQueryClassifier
from djrest.api.hay_stack.haystack.nodes.file_classifier import FileTypeClassifier
from djrest.api.hay_stack.haystack.utils import preprocessing as preprocessing
from djrest.api.hay_stack.haystack.modeling import utils as modeling_utils
from djrest.api.hay_stack.haystack.utils import cleaning as cleaning

# For the alias to work as an importable module (like `from djrest.api.hay_stack.haystack import reader`), 
# modules need to be set as attributes of their parent model.
# To make chain imports work (`from djrest.api.hay_stack.haystack.reader import FARMReader`) the module
# needs to be also present in sys.modules with its complete import path.
setattr(knowledge_graph, "graphdb", DeprecatedModule(knowledge_graph))
sys.modules["haystack.knowledge_graph.graphdb"] = DeprecatedModule(knowledge_graph)

setattr(preprocessor, "utils", DeprecatedModule(preprocessing))
setattr(preprocessor, "cleaning", DeprecatedModule(cleaning))
sys.modules["haystack.preprocessor.utils"] = DeprecatedModule(preprocessing)
sys.modules["haystack.preprocessor.cleaning"] = DeprecatedModule(cleaning)

setattr(djrest.api.hay_stack, "document_store", DeprecatedModule(document_stores))
setattr(djrest.api.hay_stack, "connector", DeprecatedModule(connector))
setattr(djrest.api.hay_stack, "generator", DeprecatedModule(generator))
setattr(djrest.api.hay_stack, "document_classifier", DeprecatedModule(document_classifier))
setattr(djrest.api.hay_stack, "extractor", DeprecatedModule(extractor))
setattr(djrest.api.hay_stack, "eval", DeprecatedModule(eval))
setattr(djrest.api.hay_stack, "file_converter", DeprecatedModule(file_converter, deprecated_attributes=["FileTypeClassifier"]))
setattr(djrest.api.hay_stack, "graph_retriever", DeprecatedModule(graph_retriever))
setattr(djrest.api.hay_stack, "knowledge_graph", DeprecatedModule(knowledge_graph, deprecated_attributes=["graphdb"]))
setattr(djrest.api.hay_stack, "pipeline", DeprecatedModule(pipelines, deprecated_attributes=["JoinDocuments", "Docs2Answers", "SklearnQueryClassifier", "TransformersQueryClassifier"]))
setattr(djrest.api.hay_stack, "preprocessor", DeprecatedModule(preprocessor, deprecated_attributes=["utils", "cleaning"]))
setattr(djrest.api.hay_stack, "question_generator", DeprecatedModule(question_generator))
setattr(djrest.api.hay_stack, "ranker", DeprecatedModule(ranker))
setattr(djrest.api.hay_stack, "reader", DeprecatedModule(reader))
setattr(djrest.api.hay_stack, "retriever", DeprecatedModule(retriever))
setattr(djrest.api.hay_stack, "summarizer", DeprecatedModule(summarizer))
setattr(djrest.api.hay_stack, "translator", DeprecatedModule(translator))
sys.modules["haystack.document_store"] = DeprecatedModule(document_stores)
sys.modules["haystack.connector"] = DeprecatedModule(connector)
sys.modules["haystack.generator"] = DeprecatedModule(generator)
sys.modules["haystack.document_classifier"] = DeprecatedModule(document_classifier)
sys.modules["haystack.extractor"] = DeprecatedModule(extractor)
sys.modules["haystack.eval"] = DeprecatedModule(eval)
sys.modules["haystack.file_converter"] = DeprecatedModule(file_converter)
sys.modules["haystack.graph_retriever"] = DeprecatedModule(graph_retriever)
sys.modules["haystack.knowledge_graph"] = DeprecatedModule(knowledge_graph)
sys.modules["haystack.pipeline"] = DeprecatedModule(pipelines)
sys.modules["haystack.preprocessor"] = DeprecatedModule(preprocessor, deprecated_attributes=["utils", "cleaning"])
sys.modules["haystack.question_generator"] = DeprecatedModule(question_generator)
sys.modules["haystack.ranker"] = DeprecatedModule(ranker)
sys.modules["haystack.reader"] = DeprecatedModule(reader)
sys.modules["haystack.retriever"] = DeprecatedModule(retriever)
sys.modules["haystack.summarizer"] = DeprecatedModule(summarizer)
sys.modules["haystack.translator"] = DeprecatedModule(translator)

# To be imported from modules, classes need only to be set as attributes, 
# they don't need to be present in sys.modules too.
# Adding them to sys.modules would enable `import haystack.pipelines.JoinDocuments`, 
# which I believe it's a very rare import style.
setattr(file_converter, "FileTypeClassifier", FileTypeClassifier)
setattr(modeling_utils, "MLFlowLogger", MLFlowLogger)
setattr(modeling_utils, "StdoutLogger", StdoutLogger)
setattr(modeling_utils, "TensorBoardLogger", TensorBoardLogger)
setattr(pipelines, "JoinDocuments", JoinDocuments)
setattr(pipelines, "Docs2Answers", Docs2Answers)
setattr(pipelines, "SklearnQueryClassifier", SklearnQueryClassifier)
setattr(pipelines, "TransformersQueryClassifier", TransformersQueryClassifier)

# This last line is used to throw the deprecation error for imports like `from djrest.api.hay_stack.haystack import connector`
deprecated_attributes=[
    "document_store", 
    "connector",
    "generator",
    "document_classifier",
    "extractor",
    "eval",
    "file_converter",
    "graph_retriever",
    "knowledge_graph",
    "pipeline",
    "preprocessor",
    "question_generator",
    "ranker",
    "reader",
    "retriever",
    "summarizer",
    "translator"
]
sys.modules["haystack"] = DeprecatedModule(djrest.api.hay_stack, is_module_deprecated=False, deprecated_attributes=deprecated_attributes)