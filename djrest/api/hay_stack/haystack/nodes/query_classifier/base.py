from abc import abstractmethod
from typing import List, Optional

from djrest.api.hay_stack.haystack.schema import Document
from djrest.api.hay_stack.haystack.nodes.base import BaseComponent


class BaseQueryClassifier(BaseComponent):
    """
    Abstract class for Query Classifiers
    """
    outgoing_edges = 2

    @abstractmethod
    def run(self, documents: List[Document], generate_single_summary: Optional[bool] = None): # type: ignore
        raise NotImplementedError()
