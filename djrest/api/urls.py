# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — API urls
# :Created:   mar 1 giu 2021, 18:41:45
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

from django.urls import include, path, re_path
from .views import BaseView, ApiKeyView, TrainView, StatusTaskView

urlpatterns = [
    path('isAlive', BaseView.as_view(), name = "isAlive"),
    path('create-apikey', ApiKeyView.as_view(), name="create-apikey"),
    path('search', TrainView.as_view(), name = "search"),
    # path('status', StatusTaskView.as_view(), name = "status"),
]
