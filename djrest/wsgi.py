# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — WSGI
# :Created:   mar 1 giu 2021, 18:15:13
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


"""
WSGI config for api project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djrest.settings')

application = get_wsgi_application()
