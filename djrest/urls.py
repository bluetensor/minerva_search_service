# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — API urls
# :Created:   mar 1 giu 2021, 18:14:10
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


from django.conf.urls import url, include
from django.contrib.auth.models import User
from django.urls import path, include
from django.views.generic import RedirectView

from rest_framework import serializers, viewsets, routers


# Wire up our API using automatic URL routing.
urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/', include('djrest.api.urls')),
    url(r'^$', RedirectView.as_view(pattern_name='isAlive', permanent=False)),
]
