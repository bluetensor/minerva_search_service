# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — Some utils for celery tasks
# :Created:   lun 7 giu 2021, 11:38:17
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

from celery.result import AsyncResult

from .tasks import test_add, test_task, webhook


def get_task_status(task_id):
    """Check status task with asyncresult"""
    # TODO: investigate how to check if task exists or not
    # because if not exists the status will be PENDING always
    return AsyncResult(task_id).state


def create_task(task, data, callback):
    """
    Create task helper
    """
    task_detail = task.apply_async((data,), link=webhook.s(callback))
    return task_detail
