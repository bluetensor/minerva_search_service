# -*- coding: utf-8 -*-
# :Project: Rest Django Webservice template — Tasks
# :Created:   ven 4 giu 2021, 12:15:44
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

import json
import pandas as pd

import requests

from django.http import HttpResponse
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connections

from djrest.api.hay_stack.hey_stack import getAnswer

from .celery import celery_app


@celery_app.task
def test_add(x, y):
    return x + y

@celery_app.task
def test_task(data):
    # Testing in celery is a little bit different
    # we have to use rdb from celery contrib to do that
    # and connect through telnet to the task
    # from celery.contrib import rdb
    # rdb.set_trace()
    return True

@celery_app.task
def train_task(data):
    risposta, intermediaHW, biblio= getAnswer(data, settings.HAYSTACK_DATADIR)

    #Spostata nel portale principale
    # with connections['minerva'].cursor() as cursor:
    #     cursor.execute("SELECT id FROM documents_document")
    #     query_result = cursor.fetchall()
    return risposta, intermediaHW, biblio

@celery_app.task
def webhook(data, target, **kwargs):
    """
    target:     the url to send the callback/hook.
    data:       a dict structure
    """
    response = requests.post(
        url=target,
        data=json.dumps(data, cls=DjangoJSONEncoder),
        headers={'Content-Type': 'application/json'}
    )
    # TODO: would be nice to log this, at least for a little while...
    return True
