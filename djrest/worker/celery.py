# -*- coding: utf-8 -*-
# :Project:   DjRest — Celery configuration
# :Created:   gio 3 giu 2021, 10:02:40
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluentesor
#


import os

from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djrest.settings')

celery_app = Celery('worker',
                    broker='redis://localhost:6379/0',
                    backend='redis://localhost:6379/0',
                    include=['djrest.worker.tasks']
                    )

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
celery_app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
celery_app.autodiscover_tasks()
