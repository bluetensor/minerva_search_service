# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — API Test
# :Created:   mar 1 giu 2021, 18:18:48
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

from time import sleep

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.utils.module_loading import import_string

from djrest.worker.tasks import test_add, test_task, webhook

from .base import BaseTestCase
from ..utils import get_task_status, create_task


class WorkerTestCase(BaseTestCase):

    def setUp(self):
        super(WorkerTestCase, self).setUp()

    def test_basic_task(self):
        """Test basic task"""
        result = test_add(4,4)
        self.assertTrue(result==8)

        result = test_add.delay(4, 4)
        sleep(0.5)
        self.assertTrue(result.get()==8)

    def test_tasks_in_chain(self):
        """Test two simple tasks in chain for use it for webhook"""

        result = test_add.apply_async((2, 2), link=test_add.s(10))
        sleep(0.5)

        # get result of children task (chained task)
        res = result.children[0].get()
        self.assertTrue(res==14)

    def test_tasks_and_webhook(self):
        """Test two simple tasks in chain for use it for webhook"""

        data = 'test'
        url = 'http://localhost:8000/api/v1/isAlive'

        # launch tasks
        result = create_task(test_task, data, url)
        # wait for task is finished
        sleep(0.5)

        # get result of children task (chained task)
        res = result.children[0].get()
        self.assertTrue(res==True)

    def test_tasks_and_webhook(self):
        """Test tasks in chain and get status of it"""

        data = dict(data='data')
        url = 'http://localhost:8000/api/v1/isAlive'

        # launch tasks
        result = create_task(test_task, data, url)
        self.assertEqual(get_task_status(result.id), 'PENDING')
        sleep(0.5)

        # get result of children task (chained task)
        res = result.children[0].get()
        self.assertTrue(res==True)
        self.assertEqual(get_task_status(result.id), 'SUCCESS')
