# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — Core App config
# :Created:   mar 1 giu 2021, 18:16:28
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'djrest.core'
