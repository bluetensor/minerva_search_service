# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — Production settings
# :Created:   mar 1 giu 2021, 18:15:55
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


from .base import *

# WARNING: with DEBUG=False the static files fail to load
DEBUG = True if 'DJANGO_DEBUG' in os.environ else False

SITE_HOME = os.environ['SITE_HOME']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6em1q-f8vi6oyzsi_uxj%1ntf2dak4e(1-dh1^_h(9y&^_ixk4'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS')
if ALLOWED_HOSTS is None:
    ALLOWED_HOSTS = ['*']
else:
    ALLOWED_HOSTS = ALLOWED_HOSTS.split()

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(SITE_HOME, 'db.sqlite3'),
    }
}

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'website', 'static'),
]

STATIC_ROOT = os.path.join(SITE_HOME, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(SITE_HOME, 'media')
MEDIA_URL = '/media/'

DEFAULT_FROM_EMAIL = SERVER_EMAIL = os.environ.get('DJANGO_SITE_FROM_EMAIL',
                                                   'webmaster@localhost')

REGISTRATION_DEFAULT_FROM_EMAIL = DEFAULT_FROM_EMAIL

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
# see https://docs.djangoproject.com/en/2.1/ref/settings/#email-backend

TIME_ZONE = os.environ.get('DJANGO_TIME_ZONE', TIME_ZONE)

# Wagtail settings

# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = os.environ.get('DJANGO_BASE_URL', BASE_URL)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'journal': {
            'class': 'logging.handlers.SysLogHandler',
            'address': '/dev/log',
            'facility': 'user',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['journal'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
    },
}

# enable/disable update check
WAGTAIL_ENABLE_UPDATE_CHECK = False


try:
    from .local import *
except ImportError:
    pass
