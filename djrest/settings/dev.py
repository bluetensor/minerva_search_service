# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template — Dev settings
# :Created:   mar 1 giu 2021, 18:15:48
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@i*tz7b-r(*7amhmw2w00v=qlb0(ugxy*mlh-(+2!z114=a0ab'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

if 'django_extensions' not in INSTALLED_APPS:
    try:
        import django_extensions  # NOQA: F401
    except ImportError:
        pass
    else:
        INSTALLED_APPS += (
            'django_extensions',
        )

try:
    from .local import *
except ImportError:
    pass
