# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template  — Makefile
# :Created:   mar 1 giu 2021, 18:12:19
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#

ACTIVATE_SCRIPT := $(VENVDIR)/bin/activate
PIP := $(VENVDIR)/bin/pip
REQUIREMENTS ?= requirements.txt
REQUIREMENTS_TIMESTAMP := $(VENVDIR)/$(REQUIREMENTS).timestamp

help::
	@printf "\nVirtualenv\n==========\n"

help::
	@printf "\nvirtualenv\n\tsetup the Python virtualenv and install required packages\n"

.PHONY: virtualenv
virtualenv: $(VENVDIR) requirements

$(VENVDIR):
	@printf "Bootstrapping Python 3 virtualenv...\n"
	@$(SYS_PYTHON) -m venv --prompt $(notdir $(TOPDIR)) $@
	@$(MAKE) -s upgrade-pip

help::
	@printf "\nupgrade-pip\n\tupgrade pip\n"

.PHONY: upgrade-pip
upgrade-pip:
	@printf "Upgrading pip...\n"
	@$(PIP) install --upgrade pip

help::
	@printf "\nrequirements\n\tinstall/update required Python packages\n"

.PHONY: requirements
requirements: $(REQUIREMENTS_TIMESTAMP)

$(REQUIREMENTS_TIMESTAMP): $(REQUIREMENTS)
	@printf "Installing development requirements...\n"
	@PATH=$(TOPDIR)/bin:$(PATH) $(PIP) install -r $(REQUIREMENTS)
	@touch $@

distclean::
	rm -rf $(VENVDIR)
