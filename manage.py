# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template  —
# :Created:   mar 1 giu 2021, 18:13:37
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djrest.settings.dev')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
