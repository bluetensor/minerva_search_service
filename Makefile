# -*- coding: utf-8 -*-
# :Project:   Rest Django Webservice template  — Makefile
# :Created:   mar 1 giu 2021, 18:12:19
# :Author:    Xavi Torné <xavitorne@gmail.com>
# :License:
# :Copyright: © 2021 Bluetensor
#


include conf/makefiles/Makefile.defs

ifndef SETTINGS
override SETTINGS = djrest.settings.dev
endif

.PHONY: all
all: virtualenv help


help::
	@printf "Application\n===========\n"

help::
	@printf "\nrunserver\n\tlaunch the webserver\n"
	@printf "\nshell_plus\n\tlaunch the shell plus\n"
	@printf "\nremove_db\n\tremove db sqlite\n"
	@printf "\ncreate_db\n\tcreate db with migrate\n"
	@printf "\nshell\n\tEnter in a shell plus\n"
	@printf "\nredis\n\tRun docker redis\n"
	@printf "\nworker\n\tRun celery worker\n"
	@printf "\nstart\n\tRun redis and worker in bg and start app\n"

help::
	@printf "Testing\n===========\n"

help::
	@printf "\ntest\n\tlaunch tests\n"
	@printf "\ncoverage\n\tcheck coverage test\n"
	@printf "\nreport\n\tshow coverage report\n"

run: ## Run the development server.
	python manage.py runserver --settings=$(SETTINGS) $(ADDRPORT)

runserver: ## Run the development server.
	python manage.py runserver --settings=$(SETTINGS) $(ADDRPORT)

shell: ## Run the shell_plus command.
	python manage.py shell_plus --settings=$(SETTINGS)

redis: ## run docker celery
	docker run -d -p 6379:6379 redis

worker: ## run worker celery
	celery -A djrest.worker worker --loglevel=INFO -n worker1

start: ## start everything
	docker run -d -p 6379:6379 redis
	celery -A djrest.worker worker -n worker1 --detach
	python manage.py runserver --settings=$(SETTINGS) $(ADDRPORT)

remove_db: ## Remove old db
	rm db.sqlite3

create_db: ## Create db and insert new data
	python manage.py migrate

clean-pyc: ## Remove Python artifacts.
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -R -f {} +


# Testing
_test-command:
	python manage.py test $(MODULE) --settings=$(SETTINGS) $(SKIPMIGRATIONS) $(DEBUG) $(ARGUMENTS)

test: ## MODULE=<python module name> - Run tests for a single app, module or test class.
test: clean-pyc _test-command

coverage: ## run coverage tests
	coverage run --source='djrest' manage.py test djrest

report:
	coverage report -m

include conf/makefiles/Makefile.virtualenv
include conf/makefiles/Makefile.ansible
